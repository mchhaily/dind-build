FROM docker:stable

RUN apk add --update curl openssh bash make git
RUN git config --global url."https://".insteadOf git://

CMD [ "sh" ]
